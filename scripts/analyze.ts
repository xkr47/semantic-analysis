import BatchAnalyzer from '../src/tools/BatchAnalyzer';
import Exporter from '../src/tools/Exporter';
import {IDocumentHandler} from '../src';
import fs, {WriteStream} from 'fs';
import * as path from 'path';
import * as readline from 'readline';

const outDir = './exports';
const initBatch: string[] = [
];


class VisitedLogger {

    /* Properties */

    private readonly visited = new Set<string>();
    private logger?: WriteStream;

    /* Initialization */

    constructor(private readonly path?: string) {
    }

    readonly setupFromFile = async (path?: string) => new Promise<boolean>(resolve => {
        const rlPath = path ?? this.path;
        if (rlPath && fs.existsSync(rlPath) && fs.lstatSync(rlPath).isFile()) {
            const readIf = readline.createInterface({input: fs.createReadStream(rlPath)});
            readIf.on('line', line => this.visited.add(line));
            readIf.on('close', () => resolve(true));
        }
        else {
            resolve(false);
        }
    });

    /* Manipulation */

    add(url: string) {
        this.visited.add(url);
        if (!this.logger && this.path) {
            this.logger = fs.createWriteStream(this.path, {flags: 'a'});
        }
        this.logger?.write(url + '\n');
    }

    readonly has = (url: string) => this.visited.has(url);

}

const ex = new Exporter(outDir);
const ba = new BatchAnalyzer({
    rateLimit: 500,
    callback: finished => finished.results && ex.export(finished.results)
        .then(exported => console.log(`[${finished.id}] Exported to ${exported.paths.json}`))
        .catch(reason => console.error(`[${finished.id}] Failed to export:`, reason)),
});

const visited = new VisitedLogger(path.join(outDir, 'visited.txt'));
visited.setupFromFile().then(() => initBatch.forEach(url => enqueue(url)));

function enqueue(url: string) {
    if (!visited.has(url)) {
        ba.addTask(url)
            .then(done => {
                const doc = done.results.document;
                if (doc) {
                    console.log(`[${done.id}] Finished analyzing ${doc.url}`);
                    getSameSiteLinks(doc).forEach(url => enqueue(url));
                }
            })
            .catch(failed => console.error(`[${failed.id}] Failed:`, failed.error));
        visited.add(url);
    }
}

function getSameSiteLinks(doc: IDocumentHandler): ReadonlyArray<string> {
    let url: URL;
    try {
        url = new URL(doc.url!);
    } catch (e) {
        console.error('Invalid URL observed', e);
        return [];
    }

    return Array.from(doc.document.querySelectorAll('a[href]') as NodeListOf<HTMLAnchorElement>)
        .map(({href}) => {
            try {
                return new URL(href, url);
            } catch (e) {
                console.error('Cannot create target URL', e);
                return undefined;
            }
        })
        .filter(target => target?.host === url.host)
        .map(target => target!.toString());
}
