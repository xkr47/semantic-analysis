import {IExtractionResult} from '../structured-data/extraction';
import fs from 'fs';
import path from 'path';
import {IResultSimple, toSerializable} from '../structured-data/item/serializable';

export interface IExportable {
    results: IResultSimple;
    url?: string;
}

export interface IExported extends IExportable {
    paths: {
        json: string;
        html?: string;
    }
}

export interface IExportFailed extends IExported {
    error: any;
}

export const prepareExport = (result: IExtractionResult): IExportable => ({
    results: toSerializable(result),
    url: result.document?.url,
});

export default class Exporter {

    /* Properties */

    public readonly extractionDirectory: string;

    /* Initialization */

    constructor(extractionDirectory: string) {
        this.extractionDirectory = path.resolve(extractionDirectory);
        if (!fs.existsSync(this.extractionDirectory)) {
            console.debug(`Creating directory ${this.extractionDirectory}`);
            fs.mkdirSync(this.extractionDirectory);
        }
    }

    /* API */

    async export(results: IExtractionResult): Promise<IExported> {
        const timestamp = Date.now();
        let safeUrl: string;
        try {
            const url = new URL(results.document!.url!);
            const urlFormat = `${url.host}/${url.pathname}${url.search}`;
            safeUrl = urlFormat.replace(/[^a-z0-9]/gi, '_').toLowerCase();
        } catch {
            safeUrl = '';
        }
        const base = `${timestamp}_${safeUrl}`;

        const source = results.document?.source;
        const jsonPath = path.join(this.extractionDirectory, `${base}.json`);
        const htmlPath = path.join(this.extractionDirectory, `${base}.html`);

        const jsonWriting = fs.promises.writeFile(
            jsonPath,
            JSON.stringify(prepareExport(results)),
            'utf8',
        );
        const htmlWriting = source ?
            fs.promises.writeFile(
                htmlPath,
                source,
                'utf8',
            ) : Promise.resolve();

        const exported: IExported = {
            ...prepareExport(results),
            paths: {
                json: jsonPath,
                html: source && htmlPath,
            }
        };

        return Promise.all([jsonWriting, htmlWriting])
            .then(() => exported)
            .catch(reason => {
                throw ({
                    error: reason,
                    ...exported
                } as IExportFailed);
            });
    }

}