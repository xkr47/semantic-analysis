import { JSDOM } from 'jsdom';


export interface IDocumentHandler {

    url?: string;
    source: string;
    document: DocumentFragment;

}

const TheParser = (() => {
    try {
        return DOMParser;
    } catch (e) {
        return new JSDOM().window.DOMParser;
    }
})();

export default class DocumentHandler implements Readonly<IDocumentHandler> {

    /* Properties */

    public readonly document: DocumentFragment;

    /* Initialization */

    constructor(
        public readonly source: string,
        public readonly url?: string,
    ) {
        this.document = new TheParser().parseFromString(source, 'text/html');
    }

}
