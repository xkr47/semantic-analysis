import {IExtractionResult} from '../structured-data/extraction';
import analysis from '../structured-data/analysis';

type ITaskBase = Readonly<{
    id: number;
    url: string;
    source?: string;
    resolver: {
        resolve: (value?: ITaskDone) => void;
        reject: (reason?: ITaskFailed) => void;
    };
    queued: Date;
    handler: Promise<IExtractionResult>;
    run: Date;
    done: Date;
    results: IExtractionResult;
    failed: Date;
    error: unknown;
}>;

export type ITask = Pick<ITaskBase, 'id' | 'url' | 'source' | 'resolver' | 'queued'> & Partial<ITaskBase>;
export type ITaskQueued = ITask;
export type ITaskRunning = ITaskQueued & Pick<ITaskBase, 'handler' | 'run'>;
export type ITaskDone = ITaskRunning & Pick<ITaskBase, 'done' | 'results'>;
export type ITaskFailed = ITaskRunning & Pick<ITaskBase, 'failed' | 'error'>;

interface IOptions {
    rateLimit: number;
    maxRunning: number;
    doneCallback?: (done: ITaskDone) => void;
    failedCallback?: (failed: ITaskFailed) => void;
    callback?: (finished: ITask) => void;
}

type TimeoutHandler = ReturnType<typeof setTimeout>;

export default class BatchAnalyzer {

    /* Properties */

    private readonly options: IOptions;

    public readonly queued: ITaskQueued[] = [];
    public readonly running: ITaskRunning[] = [];
    // public readonly done: ITaskDone[] = [];
    // public readonly failed: ITaskFailed[] = [];

    private tasksTotal = 0;
    private coolingDown?: TimeoutHandler;
    private disposed = false;

    /* Initialization */

    constructor(options?: Partial<IOptions>) {
        this.options = {
            rateLimit: 0,
            maxRunning: 1,
            ...(options ?? {})
        };
    }

    /* API */

    addTask(url: string, source?: string): Promise<ITaskDone> {
        if (this.disposed) {
            throw 'The analyzer has already been disposed, get a new one.';
        }
        const promise = new Promise<ITaskDone>((resolve, reject) => {
            const task = {
                id: this.tasksTotal++,
                url, source,
                resolver: {resolve, reject},
                queued: new Date(),
            };
            this.queued.push(task);
            this.trigger();
        });
        promise
            .then(done => {
                this.options.doneCallback?.(done);
                this.options.callback?.(done);
            })
            .catch(failed => {
                this.options.failedCallback?.(failed);
                this.options.callback?.(failed);
            });
        return promise;
    }

    private trigger() {
        if (this.coolingDown === undefined && this.running.length < this.options.maxRunning) {
            const task = this.queued.shift();
            if (task) {
                this.runTask(task);
            }
        }
    }

    private runTask(task: ITaskQueued) {
        const taskRunning: ITaskRunning = {
            handler: analysis(task.url, task.source),
            run: new Date(),
            ...task
        };
        this.running.push(taskRunning);

        taskRunning.handler
            .then(results => {
                const taskDone: ITaskDone = {
                    results,
                    done: new Date(),
                    ...taskRunning
                };
                // this.done.push(taskDone);
                taskRunning.resolver.resolve(taskDone);
            })
            .catch(error => {
                const taskFailed: ITaskFailed = {
                    error,
                    failed: new Date(),
                    ...taskRunning
                };
                // this.failed.push(taskFailed);
                taskRunning.resolver.reject(taskFailed);
            })
            .finally(() => {
                this.running.splice(this.running.indexOf(taskRunning), 1);
                this.coolDown();
            });
    }

    private coolDown() {
        this.coolingDown = this.coolingDown ?? setTimeout(() => {
            this.coolingDown = undefined;
            this.trigger();
        }, this.options.rateLimit);
    }

    /* Disposable */

    dispose() {
        if (this.coolingDown !== undefined) {
            clearTimeout(this.coolingDown);
            this.coolingDown = undefined;
        }
        this.disposed = true;
    }

}
