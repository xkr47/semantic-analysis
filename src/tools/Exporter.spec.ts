import BatchAnalyzer from './BatchAnalyzer';
import Exporter from './Exporter';


test('Invalid path', async () => {
    const done = await new BatchAnalyzer().addTask('https://lajcok.cz', `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Jiří Lajčok</title>
</head>
<body>

<section itemscope itemtype="http://schema.org/Person">
    <link itemprop="url" href="http://lajcok.cz">
    <h3 itemprop="name">Jiří Lajčok</h3>
    <p>
        Contact me at
        <a href="mailto:jiri@lajcok.cz" itemprop="email">
            jiri@lajcok.cz
        </a>
    </p>
</section>

</body>
</html>
`);

    expect.assertions(1);
    const exporter = new Exporter('nonsense:this is');
    await exporter.export(done.results)
        .catch(error => {
            expect(error).toBeTruthy();
            console.debug('Export Predictably Failed:', error);
        })
});