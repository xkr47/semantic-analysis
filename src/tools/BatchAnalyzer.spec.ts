import BatchAnalyzer from './BatchAnalyzer';

test('Analysis', async () => {
    const ba = new BatchAnalyzer();
    const done = await ba.addTask('https://lajcok.cz', `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Jiří Lajčok</title>
</head>
<body>

<section itemscope itemtype="http://schema.org/Person">
    <link itemprop="url" href="http://lajcok.cz">
    <h3 itemprop="name">Jiří Lajčok</h3>
    <p>
        Contact me at
        <a href="mailto:jiri@lajcok.cz" itemprop="email">
            jiri@lajcok.cz
        </a>
    </p>
</section>

</body>
</html>
`);

    expect(done.results).not.toBeUndefined();
    expect(ba.done.length).toEqual(1);
});