import {ILog} from './ILog';
import {IExtractor} from '../extraction';
import DocumentHandler from '../../tools/DocumentHandler';

/**
 * Additional extraction info
 */
export interface IMeta {

    /**
     * Format used to describe the structured data
     */
    type: 'JSON-LD' | 'Microdata' | 'RDFa' | 'Microformats';

    /**
     * Reference to the original extractor
     */
    extractor: IExtractor;

    /**
     * Source document of extraction
     */
    document: DocumentHandler;

    /**
     * Reference to the source element
     */
    element: Element;

    // TODO Document handler line reference

    /**
     * Logs, errors etc.
     */
    logs: ILog[];

}
