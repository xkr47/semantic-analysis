import {IExtractionResult} from '../extraction';

/**
 * Generalization of an result validator
 */
export interface IValidator {

    /**
     * Inspects given result and logs the results in the result
     *
     * @param result Result to inspect
     */
    validate(result: IExtractionResult): void;

}
