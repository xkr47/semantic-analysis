import {IValidator} from './IValidator';
import {IExtractionResult} from '../extraction';
import {IClass, IProperty, ISchema, isClass, ITerm} from './schema';
import {IItem, Spec} from '../item';
import {combine, Value} from '../item/spec';

// TODO Fix wrong property name!

/**
 * Validates result against schema.org
 */
export class SchemaValidator implements IValidator {

    /* Initialization */

    constructor(private readonly schema: ISchema) {
    }

    /* Validator */

    public validate(result: IExtractionResult) {
        console.groupCollapsed('Schema validation');
        result.items.forEach(item => this.inspect(item));
        console.groupEnd();
    }

    private inspect(item: IItem) {
        const itemTypes: ITerm[] = this.obtainClass(item);
        if (!itemTypes.length || itemTypes.find(t => !t.id.startsWith(namespaceSchema) || !(t.type && t.type.includes('Class')))) {
            return;
        }

        const domain = this.obtainDomain(
            itemTypes.filter(t => isClass(t))
        );

        // TODO Move to PropertyValidator in case term marking can be specified on item
        for (const name in item.propMeta) {
            if (!item.propMeta.hasOwnProperty(name)) {
                continue;
            }

            const def = domain[name];
            if (!def) {
                const message = `Property ${name} is not present in the type's domain`;
                console.debug(message, item);
                // TODO Use item property values wrapped log instead when implemented
                item.meta.logs.push({
                    message,
                    sort: 'Error',
                    phase: 'Semantics',
                });
                continue;
            }

            for (const prop of item.propMeta[name]) {
                const type = this.determineType(prop.value);
                const range = this.completeRange(def);

                if (
                    !type.every(t => {
                        const inherentTypes = this.superClasses(t)
                            .map(tp => tp.id);
                        return !!range.find(r =>
                            inherentTypes.includes(r.id)
                        );
                    })
                ) {
                    // TODO IMPORTANT! TEST!
                    const message = `The value is not in valid range of types`;
                    console.error(message, '\nvalue:', prop.value, '\ntype:', type, '\nrange', range);
                    prop.meta.logs.push({
                        message,
                        sort: 'Error',
                        phase: 'Semantics',
                    });
                }
            }
        }
    }

    /* Helpers */

    private obtainClass(item: IItem): ITerm[] {
        return item.type
            .filter((v, i, s) => s.indexOf(v) === i) // TODO Warning about duplicit types
            .map(type => {
                const term = this.schema.get(type);
                if (!term && type.startsWith(namespaceSchema)) {
                    const message = `Type ${type} is not a recognized term of schema.org vocabulary`;
                    if (!item.meta.logs.find(l => l.message === message)) {
                        console.debug(message);
                        item.meta.logs.push({
                            message,
                            sort: 'Error',
                            phase: 'Semantics',
                        });
                    }
                }
                return term || {id: type};
            })
            .filter(term => term);
    }

    private obtainDomain(classes: IClass[]) {
        const domain: { [id: string]: IProperty } = {};

        this.schema.getProperties(...classes)
            .forEach(p => {
                domain[p.id] = p;
            });

        return domain;
    }

    private determineType(value: Value<IItem>): ITerm[] {
        if (Spec.isItem(value)) {
            this.inspect(value);
            return this.obtainClass(value);
        }

        if (Spec.isPrimitive(value)) {
            // TODO IMPORTANT! Add type assumption algorithm
            return [this.schema.get(schemaOrg('Text'))!];
        }

        throw new TypeError('Given value is neither an item or a string');
    }

    private readonly superClasses = (type: ITerm) => {
        const classes = [type];
        const pending = classes.slice();
        while (pending.length) {
            const c = pending.shift();
            if (!c || !isClass(c) || !c.subClassOf) {
                continue;
            }
            classes.push(...c.subClassOf);
            pending.push(...c.subClassOf);
        }
        return classes;
    };

    private readonly completeRange = (defs: IProperty) =>
        ['Text', 'URL']
            .map(d => this.schema.get(schemaOrg(d))!)
            .concat(defs.rangeIncludes || []);

}

const namespaceSchema = 'http://schema.org';
const schemaOrg = (id: string) => combine(id, namespaceSchema);
