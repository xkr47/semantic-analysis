import {SchemaProvider} from './SchemaProvider';
import {isClass, isProperty} from './util';
import {IClass} from './ITerm';
import {TermLoader} from './TermLoader';
import {ISchema} from './ISchema';

/**
 * Getting term by ID
 */
test('Get by ID', () => {

    const schema: ISchema = new SchemaProvider();

    const term: IClass = schema.get('http://schema.org/Person')!;
    console.debug(term);

    expect(term).not.toBeUndefined();
    expect(term.type).toContainEqual('Class');
    expect(isClass(term)).toBeTruthy();

    expect(term.subClassOf).not.toBeUndefined();
    term.subClassOf!.forEach(t => {
        expect(t).toBeInstanceOf(TermLoader);
    });

});

/**
 * Getting properties of a class and its superiors
 */
test('Get properties', () => {

    const schema: ISchema = new SchemaProvider();
    const term: IClass = schema.get('http://schema.org/Person')!;

    const properties = schema.getProperties(term);

    expect.hasAssertions();

    properties.forEach(property => {
        console.debug(property.id);

        expect(property.type).toContainEqual('Property');
        expect(isProperty(property)).toBeTruthy();
    });

});
