import schema from './schema.json';
import {ISchema, MixedTerm} from './ISchema';
import {IClass, IProperty, TermType} from './ITerm';
import {TermLoader} from './TermLoader';
import {isClass} from './util';

export class SchemaProvider implements ISchema {

    /* Properties */

    private readonly cache = new Map<string, MixedTerm | undefined>();

    /* Schema */

    public get(id: string): MixedTerm | undefined {
        if (this.cache.has(id)) {
            return this.cache.get(id);
        }

        const data = schema['@graph'].find(t => t['@id'] === id);
        const term = data ? this.createTerm(data) : undefined;

        this.cache.set(id, term);
        return term;
    }

    public getProperties(...classes: IClass[]): IProperty[] {
        classes.forEach(c => {
            if (!isClass(c)) {
                throw new TypeError('Given term is not of class type');
            }
        });

        const stack: IClass[] = classes.slice();
        const search = new Set<string>();

        while (stack.length) {
            const c = stack.shift()!;
            search.add(c.id);
            stack.unshift(...c.subClassOf || []);
        }

        return schema['@graph']
            .filter(term => {
                const domain = term['http://schema.org/domainIncludes'];
                return domain &&
                    (Array.isArray(domain) ? domain : [domain])
                        .filter(d => search.has(d['@id']))
                        .length;
            })
            .map(term => this.get(term['@id'])!);
    }

    /* Helpers */

    private readonly createTerm = (data: object): MixedTerm => ({
        /* General */
        id: data['@id'],
        type: this.parseTypes(data['@type']),
        label: data['rdfs:label'],
        comment: data['rdfs:comment'],
        supersededBy: this.parseIDs(data['http://schema.org/supersededBy']),

        /* Class */
        subClassOf: this.parseIDs(data['rdfs:subClassOf']),

        /* Property */
        rangeIncludes: this.parseIDs(data['http://schema.org/rangeIncludes']),
        domainIncludes: this.parseIDs(data['http://schema.org/domainIncludes']),
    });

    private readonly parseIDs = (data?: object): MixedTerm[] | undefined =>
        data
            ? (Array.isArray(data) ? data : [data])
                .filter(t => typeof t['@id'] === 'string')
                .map(t => new TermLoader(t['@id'], this))
            : undefined;

    private readonly parseTypes = (type?: string | string[]): TermType[] | undefined =>
        type
            ? (Array.isArray(type) ? type : [type])
                .map(t => {
                    switch (t) {
                        case 'rdfs:Class': return 'Class';
                        case 'rdf:Property': return 'Property';
                        default: return new TermLoader(t, this);
                    }
                })
                .filter(t => t) as TermType[]
            : undefined;

}
