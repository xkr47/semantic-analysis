import {IClass, IProperty, ITerm} from './ITerm';

export type MixedTerm = ITerm | IClass | IProperty;

/**
 * Schema structure used in program
 * @see https://schema.org/docs/datamodel.html
 */
export interface ISchema {

    // TODO Implement due to what's needed in IValidator

    /**
     * Get term by its ID
     * @param id ID to search for
     */
    get(id: string): MixedTerm | undefined;

    /**
     * Get properties belonging to the set of classes
     * @param classes Classes specifying domains
     */
    getProperties(...classes: IClass[]): IProperty[];

}
