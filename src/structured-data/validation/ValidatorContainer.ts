import {IValidator} from './IValidator';
import {IExtractionResult} from '../extraction';

/**
 * Validator container to specify validation process
 */
export class ValidatorContainer implements IValidator {

    /* Properties */

    private readonly validators: IValidator[];

    /* Initialization */

    constructor(...validators: IValidator[]) {
        this.validators = validators;
    }


    /* Manipulation */

    public add(...validators: IValidator[]) {
        this.validators.push(...validators);
        return this;
    }

    /* Validator */

    public readonly validate = (result: IExtractionResult) =>
        this.validators.forEach(v => v.validate(result));

}