import {IExtractionResult, IExtractor} from "./IExtractor";
import {IItem, IMeta, IMetaProperties, IMetaProperty, ILog, Spec} from '../item';
import '../../util';
import DocumentHandler from '../../tools/DocumentHandler';
import {combine} from '../item/spec';
import {map} from '../../util';

/**
 * Microdata extraction according to HTML specification
 * @see https://html.spec.whatwg.org/multipage/microdata.html#converting-html-to-other-formats
 */
export class MicrodataExtractor implements IExtractor {

    /* Properties */

    private readonly meta: Pick<IMeta, 'type' | 'extractor' | 'document'>;

    /* Initialization */

    constructor(private readonly doc: DocumentHandler) {
        this.meta = {
            type: 'Microdata',
            extractor: this,
            document: doc,
        };
    }

    /* Extractor */

    /**
     * Extracts items according to HTML specification in format similar to JSON
     * @see https://html.spec.whatwg.org/multipage/microdata.html#json
     * @returns Returns an extraction result
     */
    public extract(): Readonly<IExtractionResult> {
        console.groupCollapsed('Microdata extraction');
        const result = this.findItems();
        console.groupEnd();
        return result;
    }

    /**
     * Finds top-level items
     * @see https://html.spec.whatwg.org/multipage/microdata.html#json
     */
    private findItems = () => ({
        /**
         * @see https://html.spec.whatwg.org/multipage/microdata.html#top-level-microdata-items
         * @see https://html.spec.whatwg.org/multipage/microdata.html#microdata-and-other-namespaces
         * TODO Handle only valid HTML elements
         */
        items: Array.from(this.doc.document.querySelectorAll('[itemscope]:not([itemprop])'))
            .map(element => this.createItem(element)),
        logs: [],
        document: this.doc,
    });

    /**
     * Creates a new item
     * @see https://html.spec.whatwg.org/multipage/microdata.html#get-the-object
     * @param element Element to create item from
     * @param memory Memory of used elements
     */
    private createItem = (element: Element, memory: Element[] = []): MicrodataItem => {
        console.debug('Found an item at: ', element);

        memory.push(element);

        const item = new MicrodataItem({
            type: this.parseType(element),
            id: element.getAttribute('itemid') || undefined,
            meta: {
                ...this.meta,
                element,
                logs: [],
            },
        });

        const properties = this.findProperties(element);

        console.groupCollapsed('Property assignment');

        for (const property of properties) {
            console.debug('Inspecting property on:', property);
            let value: Spec.Value;
            const logs: ILog[] = [];

            if (property.hasAttribute('itemscope')) {
                if (!memory.includes(property)) {
                    value = this.createItem(property, memory.slice());
                } else {
                    console.debug('Property already in memory:', property);
                    console.debug('Memory:', memory);

                    logs.push({
                        message: 'Current element has already been inspected, ' +
                            'this might be a sign of cycles in the graph',
                        sort: 'Error',
                        phase: 'Extraction',
                        element: property,
                    });

                    value = 'ERROR';
                }
            } else {
                value = extractValue(property, this.doc.url);
            }

            const names = separateTokens(property.getAttribute('itemprop'))
                .filter((token, index, self) => self.indexOf(token) === index)
                .map(token => {
                    const combos = item.type
                        .map(type => combine(token, type))
                        .filter((v, i, s) => s.indexOf(v) === i);

                    if(combos.length === 1) {
                        return combos[0];
                    }

                    if (combos.length > 1) {
                        const message = 'Detected property name ambiguity against the types';
                        console.debug(`${message}, possibilities:`, combos);
                        logs.push({
                            message,
                            sort: 'Error',
                            phase: 'Extraction',
                            element: property,
                        });
                    }

                    return token;
                });

            item.addProperty(names, value, {
                ...this.meta,
                element: property,
                logs,
            });
            console.debug('Found properties', names, 'with a value:', value);
        }
        console.groupEnd();

        return item;
    };

    /**
     * Parse types of an item element
     * @see https://html.spec.whatwg.org/multipage/microdata.html#item-types
     * @param element Element to parse
     */
    private parseType = (element: Element): string[] =>
        separateTokens(element.getAttribute('itemtype'))
            .filter((value, index, self) => self.indexOf(value) === index);  // Unique only

    /**
     * Find properties in root
     * @see https://html.spec.whatwg.org/multipage/microdata.html#the-properties-of-an-item
     * @param root Element to start
     */
    private findProperties = (root: Element): Element[] => {
        console.groupCollapsed('Searching properties...');

        const results = [];
        const memory = [root];
        const pending = Array.from(root.children) as Element[];

        separateTokens(root.getAttribute('itemref'))
            .forEach(id => {
                const element = root.ownerDocument ? root.ownerDocument.getElementById(id) : undefined;
                if (element) {
                    /* tslint:disable-next-line:no-bitwise */
                    const action = element.compareDocumentPosition(root) & Node.DOCUMENT_POSITION_FOLLOWING
                        ? 'unshift' : 'push';
                    pending[action](element);
                }
            });

        while (pending.length) {
            const current = pending.shift()!;

            if (memory.includes(current)) {
                continue;
            }

            memory.push(current);

            if (!current.hasAttribute('itemscope')) {
                pending.unshift(...Array.from(current.children));
            }

            if (separateTokens(current.getAttribute('itemprop')).length > 0) {
                results.push(current);
            }
        }

        console.debug(`Found ${results.length} properties:`, results);
        console.groupEnd();

        return results;
    };

}

/**
 * Local implementation of item
 * TODO Extract into a separate package
 */
class MicrodataItem implements IItem {

    /* Properties */

    public type: string[];
    public id?: string;

    public meta: IMeta;
    public propMeta: IMicrodataProperties;

    public get properties(): Spec.IProperties<MicrodataItem> {
        return map(
            this.propMeta,
            props => props.map(prop => prop.value),
        );
    }

    /* Initialization */

    constructor(source: Partial<IItem> & Pick<IItem, 'meta'>) {
        this.type = source.type ? source.type.slice() : [];
        this.id = source.id;
        this.meta = source.meta;

        if (source.propMeta) {
            this.propMeta = map(
                source.propMeta,
                props => props.map(prop => ({
                    name: prop.name,
                    value: this.parse(prop.value),
                    meta: prop.meta,
                })),
            );
        } else if (source.properties) {
            this.propMeta = map(
                source.properties,
                values => values.map(value => ({
                        name,
                        value: this.parse(value),
                        meta: this.meta,
                    }),
                ),
            );
        } else {
            this.propMeta = {};
        }
    }


    /* Manipulation */

    public addProperty(
        name: string | string[],
        value: Spec.Value,
        meta: IMeta,
    ): this {
        (Array.isArray(name) ? name : [name])
            .forEach(n => {
                if (!this.propMeta.hasOwnProperty(n)) {
                    this.propMeta[n] = [];
                }
                this.propMeta[n].push({
                    name: n,
                    value: this.parse(value),
                    meta,
                });
            });

        return this;
    }

    /* Helpers */

    private readonly parse = (value: Spec.Value, meta?: IMeta): Spec.Value<MicrodataItem> => {
        if (Spec.isItem(value)) {
            return new MicrodataItem({...value, meta: meta ?? this.meta});
        }
        if (Spec.isPrimitive(value)) {
            return value;
        }
        throw new TypeError(`Invalid value type ${typeof value}`);
    }

}

interface IMicrodataProperties extends IMetaProperties<MicrodataItem> {
    [key: string]: Array<IMetaProperty<MicrodataItem>>;
}

/**
 * Separate tokens by whitespace
 * @param value Input to parse
 */
const separateTokens = (value: string | null | undefined): string[] =>
    (value || '').match(/\S+/g) || [];

/**
 * Extract the value of the property
 * @see https://html.spec.whatwg.org/multipage/microdata.html#values
 * @param element Element to examine for value
 * @param base string Base URL to apply to relative URLs
 */
function extractValue(element: Element, base?: string): string {
    type extractor = (x: Element) => string;

    const attr = (x: Element, name?: string) =>
        x.getAttribute(name || '') || '';

    const text = (x: Element) => (x.textContent || '').trim().replace(/\s+/g, ' ');

    /**
     * @see https://html.spec.whatwg.org/multipage/text-level-semantics.html#datetime-value
     * @param x
     */
    const datetime = (x: Element) =>
        x.hasAttribute('datetime')
            ? attr(x, 'datetime')
            : text(x);

    const completeURL = (url: string): string => {
        try {
            return new URL(url, base).toString();
        } catch (e) {
            return url;
        }
    };

    const resolver: { [key: string]: extractor } = {
        content: x => attr(x, 'content'),
        src: x => completeURL(attr(x, 'src')),
        href: x => completeURL(attr(x, 'href')),
        data: x => attr(x, 'data'),
        value: x => attr(x, 'value'),
        datetime: x => datetime(x),
        text: x => text(x),
    };

    const tagMap: { [tagName: string]: extractor } = {
        meta: resolver.content,

        audio: resolver.src,
        embed: resolver.src,
        iframe: resolver.src,
        img: resolver.src,
        source: resolver.src,
        track: resolver.src,
        video: resolver.src,

        a: resolver.href,
        area: resolver.href,
        link: resolver.href,

        object: resolver.data,

        meter: resolver.value,

        time: resolver.datetime,

        other: resolver.text,
    };

    return (tagMap[element.tagName.toLowerCase()] || tagMap.other)(element);
}
