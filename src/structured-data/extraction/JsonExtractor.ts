import {IExtractionResult, IExtractor} from "./IExtractor";
import {
    IItem,
    IMeta,
    IMetaProperties,
    IMetaProperty,
    Spec,
} from "../item";
import '../../util';
import DocumentHandler from '../../tools/DocumentHandler';
import {combine} from '../item/spec';
import {filter, map} from '../../util';

/**
 * Simplified JSON-LD extractor based on Google structured data examples
 * @see https://developers.google.com/search/docs/guides/intro-structured-data
 */
export class JsonExtractor implements IExtractor {

    /* Properties */

    private readonly meta: Pick<IMeta, 'type' | 'extractor' | 'document'>;

    /* Initialization */

    constructor(private readonly doc: DocumentHandler) {
        this.meta = {
            type: 'JSON-LD',
            extractor: this,
            document: doc,
        };
    }

    /* Extractor */

    public extract(): IExtractionResult {
        console.groupCollapsed('JSON-LD extraction');
        console.debug('Initializing...');
        const result = this.findItems();
        console.groupEnd();
        return result;
    }

    private findItems() {
        const result: IExtractionResult = {items: [], logs: [], document: this.doc};
        const scripts = this.doc.document.querySelectorAll('script[type="application/ld+json"]');

        for (const script of Array.from(scripts)) {
            let json: object;
            try {
                json = JSON.parse(script.innerHTML);
            } catch (e) {
                const message = 'Contents could not be parsed as JSON';
                console.debug(message);
                result.logs.push({
                    message,
                    sort: 'Error',
                    phase: 'Parsing',
                    element: script,
                });
                continue;
            }

            if (typeof json !== 'object' || json === null) {
                const message = `Invalid type ${json !== null ? typeof json : 'null'}, expected non-null object or array`;
                console.debug(message);
                result.logs.push({
                    message,
                    sort: 'Error',
                    phase: 'Extraction',
                    element: script,
                });
                continue;
            }

            (Array.isArray(json) ? json : [json])
                .filter(o => {
                    if (typeof o === 'object' && o !== null) {
                        return true;
                    } else {
                        const message = `Skipping ${o !== null ? typeof o : 'null'}, expected non-null object`;
                        console.debug(message);
                        result.logs.push({
                            message,
                            sort: 'Warning',
                            phase: 'Extraction',
                            element: script,
                        });
                        return false;
                    }
                })
                .forEach(o => {
                    console.debug('Building item from: ', o);
                    result.items.push(
                        new JsonItem(o, {
                            ...this.meta,
                            element: script,
                            logs: [],
                        }),
                    );
                });
        }

        console.debug('Extractions finished');

        return result;
    };
}

// TODO Move classes into a separate package

/**
 * Local implementation of an item
 */
class JsonItem implements IItem {

    /* Properties */

    private readonly builder: JsonItemBuilder;

    private _item?: IItem;

    private get item() {
        if (!this._item) {
            this._item = this.builder.build();
        }
        return this._item;
    };

    /* Initialization */

    constructor(json: object, meta: IMeta) {
        this.builder = new JsonItemBuilder(json, meta);
    }

    /* Extraction Item */

    public get type() {
        return this.item.type;
    }

    public get id() {
        return this.item.id;
    }

    public get properties() {
        return this.item.properties;
    }

    public get meta() {
        return this.item.meta;
    }

    public get propMeta() {
        return this.item.propMeta;
    }

}

class JsonItemBuilder implements IItem {

    /* Properties */

    private readonly context?: string;

    /* Initialization */

    constructor(
        private readonly json: object,
        public readonly meta: IMeta,
        context?: string,
    ) {
        if (context) {
            this.context = context;
        } else {

            const c = json['@context'];

            if (typeof c === 'string') {

                try {
                    this.context = new URL(c).toString();
                } catch (e) {
                    const message = 'Given context is not a valid URL, ignoring';
                    console.debug(message);
                    meta.logs.push({
                        message,
                        sort: 'Error',
                        element: meta.element,
                        phase: 'Extraction',
                    });
                }

            } else if (c !== undefined) {

                const message = 'The app cannot inspect non-string contexts yet';
                console.error(message);
                meta.logs.push({
                    message,
                    sort: 'Notice',
                    element: meta.element,
                    phase: 'Extraction',
                });

            }
        }

    }

    /* Item */

    public get type() {
        return this.applyContext(this.parseType());
    }

    public get id() {
        return typeof this.json['@id'] === 'string' ? this.json['@id'] : undefined;
    }

    public get properties(): Spec.IProperties<JsonItemBuilder> {
        return unwrapProperties(this.propMeta);
    }

    public get propMeta(): IMetaProperties<JsonItemBuilder> {
        return this.findProperties();
    }

    /* Build */

    public readonly build = (): IItem => ({
        type: this.type,
        id: this.id,
        get properties() {
            return unwrapProperties(this.propMeta);
        },
        meta: {...this.meta, logs: []},
        propMeta: map(
            this.propMeta,
            props => props.map(
                prop => ({
                    ...prop,
                    value: prop.value instanceof JsonItemBuilder
                        ? prop.value.build()
                        : prop.value,
                } as IMetaProperty),
            ),
        ),
    });

    /* Helpers */

    private parseType = (): string[] => {
        const type: unknown = this.json['@type'];
        if (!type) {
            return [];
        }

        if (Array.isArray(type)) {
            return type.map(t => String(t));
        } else {
            return [String(type)];
        }
    };

    private applyContext = (type: string[]) =>
        type.map(t => combine(t, this.context));

    private findProperties = (): IMetaProperties<JsonItemBuilder> => {
        const properties: any = {};
        map(
            filter(this.json, ((value, key) => !String(key).startsWith('@'))),
            (val, name) => {
                const completeName = combine(String(name), this.context);
                const values = (Array.isArray(val) ? val : [val])
                    .filter((value: unknown) =>
                        value !== null &&
                        String(value).trim() !== ''
                    )
                    .map((value: unknown) => ({
                        name: completeName,
                        value: typeof value === 'object'
                            ? new JsonItemBuilder(value!, this.meta, this.context)
                            : String(value).trim(),
                        meta: {...this.meta, logs: []},
                    }));
                properties[completeName] = values;
                return values;
            });

        return properties;
    }

}

const unwrapProperties =
    <T extends Spec.IItem = IItem>(
        propMeta: IMetaProperties<T>,
    ): Spec.IProperties<T> =>
        map(propMeta, props =>
            props.map(prop => prop.value),
        );
