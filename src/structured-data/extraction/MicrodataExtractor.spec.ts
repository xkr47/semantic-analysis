import DocumentHandler from "../../tools/DocumentHandler";
import {IExtractionResult} from "./IExtractor";
import {MicrodataExtractor} from "./MicrodataExtractor";
import {IItem} from "../item/spec";

function testOutput(source: string, expected: Pick<IExtractionResult<IItem>, 'items'>) {
    const doc = new DocumentHandler(source, 'http://example.com');
    const res = new MicrodataExtractor(doc).extract();

    expect(res).toMatchObject(expected);
}

/**
 * Testing microdata extraction from HTML source by comparing output to the expected one
 */
test('No microdata', () => {

    const source = `
<div>
    <span>propValue</span>
    <span>propValue2</span>
</div>
`;

    testOutput(source, {
        items: [],
    });
});

/**
 * @see https://html.spec.whatwg.org/multipage/microdata.html#the-basic-syntax
 */
test('Basic syntax, properties', () => {

    const source = `
<div itemscope>
    <span itemprop="propName">propValue</span>
    <span itemprop="propName2">propValue2</span>
</div>
<div itemscope>
    <span itemprop="propName propName2">propValue</span>
</div>
<div itemscope>
    <span itemprop="propName propName2">propValue</span>
    <span itemprop="propName2">propValue2</span>
</div>
<div itemscope>
    <span itemprop="propName">propValue</span>
    <span itemprop="propName">prop<strong>Value</strong></span>
</div>
`;

    testOutput(source, {
        items: [
            {
                type: [],
                properties: {
                    propName: [
                        'propValue',
                    ],
                    propName2: [
                        'propValue2',
                    ],
                },
            },
            {
                type: [],
                properties: {
                    propName: [
                        'propValue',
                    ],
                    propName2: [
                        'propValue',
                    ],
                },
            },
            {
                type: [],
                properties: {
                    propName: [
                        'propValue',
                    ],
                    propName2: [
                        'propValue',
                        'propValue2',
                    ],
                },
            },
            {
                type: [],
                properties: {
                    propName: [
                        'propValue',
                        'propValue',
                    ],
                },
            },
        ],
    });

});

/**
 * @see https://html.spec.whatwg.org/multipage/microdata.html#typed-items
 * @see https://html.spec.whatwg.org/multipage/microdata.html#global-identifiers-for-items
 */
test('Typed items, ids', () => {

    const source = `
<section itemscope itemtype="https://example.org/animals#cat" itemid="catid:123456">
     <h1 itemprop="name">Hedral</h1>
     <p itemprop="desc">
        Hedral is a male american domestic shorthair, with a fluffy black fur with white paws and belly.
     </p>
     <img itemprop="img" src="hedral.jpeg" alt="" title="Hedral, age 18 months">
</section>
`;

    testOutput(source, {
        items: [
            {
                type: ['https://example.org/animals#cat'],
                id: 'catid:123456',
                properties: {
                    'https://example.org/animals#name': ['Hedral'],
                    'https://example.org/animals#desc': ['Hedral is a male american domestic shorthair, with a fluffy black fur with white paws and belly.'],
                    'https://example.org/animals#img': ['http://example.com/hedral.jpeg'],
                }
            },
        ],
    });

});

/**
 * @see https://html.spec.whatwg.org/multipage/microdata.html#selecting-names-when-defining-vocabularies
 */
test('Property name quirks', () => {

    const source = `
<section itemscope itemtype="https://example.org/animals#cat">
     <h1 itemprop="name https://example.com/fn">Hedral</h1>
     <p itemprop="desc">
         Hedral is a male american domestic shorthair, with a fluffy
         <span itemprop="https://example.com/color">black</span> fur with
         <span itemprop="https://example.com/color">white</span> paws and belly.
     </p>
     <img itemprop="img" src="hedral.jpeg" alt="" title="Hedral, age 18 months">
</section>
`;

    testOutput(source, {
        items: [
            {
                type: ['https://example.org/animals#cat'],
                properties: {
                    'https://example.org/animals#name': ['Hedral'],
                    'https://example.com/fn': ['Hedral'],
                    'https://example.org/animals#desc': ['Hedral is a male american domestic shorthair, with a fluffy black fur with white paws and belly.'],
                    'https://example.com/color': ['black', 'white'],
                    'https://example.org/animals#img': ['http://example.com/hedral.jpeg'],
                }
            },
        ],
    });

});

/**
 * @see https://html.spec.whatwg.org/multipage/microdata.html#names:-the-itemprop-attribute
 */
test('References', () => {

    const source = `
<div id="x">
    <p itemprop="a">1</p>
</div>
<div itemscope itemref="x y">
    <p itemprop="b">test</p>
    <p itemprop="a">2</p>
</div>
<div id="y">
    <p itemprop="a">3</p>
</div>
`;

    testOutput(source, {
        items: [
            {
                type: [],
                properties: {
                    a: ['1', '2', '3'],
                    b: ['test'],
                },
            },
        ],
    });

});

/**
 * TODO Make the URLs absolute. In validation layer?
 * TODO Validate URLs. In validation layer?
 * @see https://html.spec.whatwg.org/multipage/microdata.html#json
 */
test('Test spec markup', () => {

    const source = `
<!DOCTYPE HTML>
<html lang="en">
<title>My Blog</title>
<article itemscope itemtype="http://schema.org/BlogPosting">
 <header>
  <h1 itemprop="headline">Progress report</h1>
  <p><time itemprop="datePublished" datetime="2013-08-29">today</time></p>
  <link itemprop="url" href="?comments=0">
 </header>
 <p>All in all, he's doing well with his swim lessons. The biggest thing was he had trouble
 putting his head in, but we got it down.</p>
 <section>
  <h1>Comments</h1>
  <article itemprop="comment" itemscope itemtype="http://schema.org/UserComments" id="c1">
   <link itemprop="url" href="#c1">
   <footer>
    <p>Posted by: <span itemprop="creator" itemscope itemtype="http://schema.org/Person">
     <span itemprop="name">Greg</span>
    </span></p>
    <p><time itemprop="commentTime" datetime="2013-08-29">15 minutes ago</time></p>
   </footer>
   <p>Ha!</p>
  </article>
  <article itemprop="comment" itemscope itemtype="http://schema.org/UserComments" id="c2">
   <link itemprop="url" href="#c2">
   <footer>
    <p>Posted by: <span itemprop="creator" itemscope itemtype="http://schema.org/Person">
     <span itemprop="name">Charlotte</span>
    </span></p>
    <p><time itemprop="commentTime" datetime="2013-08-29">5 minutes ago</time></p>
   </footer>
   <p>When you say "we got it down"...</p>
  </article>
 </section>
</article>
`;

    testOutput(source, {
        "items": [
            {
                "type": [ "http://schema.org/BlogPosting" ],
                "properties": {
                    "http://schema.org/headline": [ "Progress report" ],
                    "http://schema.org/datePublished": [ "2013-08-29" ],
                    "http://schema.org/url": [ "http://example.com/?comments=0" ],
                    "http://schema.org/comment": [
                        {
                            "type": [ "http://schema.org/UserComments" ],
                            "properties": {
                                "http://schema.org/url": [ "http://example.com/#c1" ],
                                "http://schema.org/creator": [
                                    {
                                        "type": [ "http://schema.org/Person" ],
                                        "properties": {
                                            "http://schema.org/name": [ "Greg" ]
                                        }
                                    }
                                ],
                                "http://schema.org/commentTime": [ "2013-08-29" ]
                            }
                        },
                        {
                            "type": [ "http://schema.org/UserComments" ],
                            "properties": {
                                "http://schema.org/url": [ "http://example.com/#c2" ],
                                "http://schema.org/creator": [
                                    {
                                        "type": [ "http://schema.org/Person" ],
                                        "properties": {
                                            "http://schema.org/name": [ "Charlotte" ]
                                        }
                                    }
                                ],
                                "http://schema.org/commentTime": [ "2013-08-29" ]
                            }
                        }
                    ]
                }
            }
        ]
    });

});

// TODO Add test for itemscope on invalid elements
// TODO Add test for property content evaluation based on tag
