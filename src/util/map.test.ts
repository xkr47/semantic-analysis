import {map} from "./map";

test('Basic functionality', () => {

    interface IMyType {
        a: string,
        b: number,
    }

    const obj: IMyType = {
        a: 'a',
        b: 2,
    };

    const res = map(obj, (value, key) => {
        if(key === 'a' && typeof value === 'string') {
            return value.length + 1;
        }
        if(key === 'b' && typeof value === 'number') {
            return value.toString() + 'b';
        }
        return fail(`Given property ${key} was not expected to be of ${typeof value} type`);
    });

    expect(res).toMatchObject({
        a: 2,
        b: '2b',
    });

});
