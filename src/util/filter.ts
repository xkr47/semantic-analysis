type Filter<T> = (value: Readonly<T[keyof T]>, key: Readonly<keyof T>, self: Readonly<T>) => boolean;
type Filtered<T, F extends Filter<T>> =
    ReturnType<F> extends true ? T :
        ReturnType<F> extends false ? Object :
            Partial<T>;

/**
 * Filters desired properties out of the object
 *
 * @param object Input object
 * @param callback Filter callback function
 */
export function filter<
    T extends object,
    F extends Filter<T> = Filter<T>,
>(
    object: T,
    callback: Filter<T>,
): Filtered<T, F> {
    const result: any = {};
    for(const key in object) {
        if(object.hasOwnProperty(key) && callback(object[key], key, object)) {
            result[key] = object[key];
        }
    }
    return result;
}
